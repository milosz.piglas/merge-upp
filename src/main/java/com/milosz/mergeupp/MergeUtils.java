package com.milosz.mergeupp;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class MergeUtils {

  private static final Logger LOG = LogManager.getLogManager().getLogger("");

  public static List<DataSheet> loadRows(File f) throws Exception {
    try (var wb = WorkbookFactory.create(f)) {
      final var input = new ArrayList<DataSheet>();
      for (final var sheet : wb) {
        final var dt = new ArrayList<Object[]>();
        for (final var r : sheet) {
          final var dr = new Object[r.getLastCellNum()];
          for (var i = 0; i < dr.length; i++) {
            dr[i] = value(r.getCell(i));
          }
          dt.add(dr);
        }
        input.add(new DataSheet(sheet.getSheetName(), f.getAbsolutePath(), dt));
      }
      return input;
    }
  }

  private static boolean isHeader(Object[] r) {
    final var s = String.valueOf(r[0]);
    return s.startsWith("Recenzowane") || s.startsWith("Monografie");
  }

  private static boolean isDataRow(Object[] r) {
    if (r[0] instanceof Double && r[1] != null) {
      return true;
    }
    if (r[0] instanceof Double) {
      LOG.info("Empty data row " + Arrays.toString(r));
      return false;
    }
    if (r[0] != null) {
      try {
        Integer.valueOf(String.valueOf(r[0]).trim().replace(".", ""));
        if (r[1] != null) {
          return true;
        }
      } catch (final Exception e) {
        LOG.info("Invalid format of 'LP' column' " + Arrays.toString(r));
        return false;
      }
    }
    return false;
  }

  public static Map<String, List<Object[]>> loadYears(DataSheet ds) {
    final var data = ds.data();
    final var years = new HashMap<String, List<Object[]>>();
    var header = false;
    List<Object[]> rows = null;
    for (final var r : data) {
      if (isHeader(r)) {
        LOG.info("Header " + Arrays.toString(r));
        header = true;
        rows = null;
      } else if (header) {
        final var y = String.valueOf(((Double) r[0]).intValue());
        rows = new ArrayList<>();
        years.put(y, rows);
        header = false;
        LOG.info("Year row: " + Arrays.toString(r));
      } else if (isDataRow(r)) {
        rows.add(r);
      } else {
        LOG.info("Skip row " + Arrays.toString(r));
      }
    }
    return years;
  }

  private static Object value(Cell c) {
    if (c == null) {
      return null;
    }
    switch (c.getCellType()) {
      case BLANK:
        return null;
      case NUMERIC:
        return c.getNumericCellValue();
      case STRING:
        return c.getStringCellValue();
      default:
        return null;
    }
  }

  private static class Row {
    private final Object[] values;
    private final String id;

    public Row(Object[] values) {
      this.values = values;
      id = calcId(values[1].toString());
    }

    private String calcId(String in) {
      final var chars = in.chars().filter(Character::isLetterOrDigit).sorted().boxed().toList();
      final var sorted = new char[chars.size()];
      for (var i = 0; i < chars.size(); i++) {
        sorted[i] = (char) chars.get(i).intValue();
      }
      return String.valueOf(sorted);
    }

    public boolean isPaid() {
      if (values[3] == null) {
        return false;
      }
      if (values[8] == null) {
        return false;
      }
      final var a = values[3].toString().charAt(0);
      return a == 'T' || a == 't';
    }

    @Override
    public int hashCode() {
      return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final var other = (Row) obj;
      return id.equals(id);
    }
  }

  private static final String[] HEADERS = {
    "LP",
    "Dane publikacji: Autorzy, Rok, Tytuł, Czasopismo, Tom, Strony, DOI",
    "ROK publikacji",
    "OPEN ACCESS",
    "Bezkosztowa",
    "Środki artykuł 365",
    "Środki z projektu",
    "inne",
    "Koszt brutto",
    "Uwagi"
  };

  private static class YearGroup {
    Map<String, Set<Row>> years = new HashMap<>();

    void add(PubInfo i) {
      final var sh = years.getOrDefault(i.year(), new HashSet<>());
      for (final var d : i.data()) {
        final var r = new Row(d);
        final var found = sh.contains(r);
        if (found && r.isPaid()) {
          sh.remove(r);
          sh.add(r);
          LOG.info(Arrays.toString(r.values) + " replaces existing row");
        } else if (!found) {
          sh.add(r);
        } else if (found) {
          LOG.info("Duplicate " + Arrays.toString(r.values) + " " + r.id);
        }
      }
      years.put(i.year(), sh);
    }

    void addRows(Sheet sheet) {
      var rn = 0;
      for (final var ey : years.entrySet()) {
        sheet.createRow(rn++).createCell(0);
        sheet.createRow(rn++).createCell(0).setCellValue(sheet.getSheetName());
        sheet.createRow(rn++).createCell(0).setCellValue(ey.getKey());
        final var hr = sheet.createRow(rn++);
        hr.setHeightInPoints(20);
        for (var h = 0; h < HEADERS.length; h++) {
          final var c = hr.createCell(h);
          c.setCellValue(HEADERS[h]);
          sheet.setColumnWidth(h, Math.max(HEADERS[h].length(), 15) * 256);
        }
        var yn = 1;
        for (final var dr : ey.getValue()) {
          final var nrow = sheet.createRow(rn);
          for (var ci = 0; ci < dr.values.length; ci++) {
            final var nc = nrow.createCell(ci);
            if (dr.values[ci] != null) {
              if (ci > 0) {
                nc.setCellValue(String.valueOf(dr.values[ci]));
              } else {
                nc.setCellValue(yn++);
              }
            }
          }
          rn++;
        }
      }
    }
  }

  public static void mergeAndSave(List<PubInfo> info, File file) throws Exception {
    final var sheets = new HashMap<String, YearGroup>();
    for (final PubInfo i : info) {
      final var yg = sheets.getOrDefault(i.sheet(), new YearGroup());
      yg.add(i);
      sheets.put(i.sheet(), yg);
    }
    try (var wb = WorkbookFactory.create(true)) {

      for (final var ent : sheets.entrySet()) {
        ent.getValue().addRows(wb.createSheet(ent.getKey()));
      }
      final var fow = new FileOutputStream(file);
      wb.write(fow);
      fow.close();
    }
  }
}
