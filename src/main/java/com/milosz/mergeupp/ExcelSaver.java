package com.milosz.mergeupp;

import java.io.File;
import java.util.Arrays;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.SwingWorker;

public class ExcelSaver extends SwingWorker<Integer, String> {

  private static final Logger LOG = LogManager.getLogManager().getLogger("");

  private final File file;
  private final SheetModel model;

  public ExcelSaver(File file, SheetModel model) {
    this.file = file;
    this.model = model;
  }

  @Override
  protected Integer doInBackground() throws Exception {
    try {
      MergeUtils.mergeAndSave(model.getSections(), file);
      LOG.info("Saved file " + file.getAbsolutePath());
    } catch (final Exception e) {
      LOG.info(
          "Save error "
              + e.getLocalizedMessage()
              + " stack: "
              + Arrays.stream(e.getStackTrace())
                  .map(String::valueOf)
                  .collect(Collectors.joining("#")));
      e.printStackTrace();
    }
    return 0;
  }
}
