package com.milosz.mergeupp;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {

  private static final String SEP = System.getProperty("line.separator");

  @Override
  public String format(LogRecord record) {

    return String.format(
        "[%1$tY-%1$td-%1$tdT%1$tH:%1$tM:%1$tS] %2$s.%3$s %4$s " + SEP,
        record.getMillis(),
        record.getSourceClassName(),
        record.getSourceMethodName(),
        record.getMessage());
  }
}
