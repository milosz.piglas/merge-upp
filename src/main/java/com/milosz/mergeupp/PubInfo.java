package com.milosz.mergeupp;

import java.util.List;

public record PubInfo(String path, String sheet, String year, int rows, List<Object[]> data) {}
