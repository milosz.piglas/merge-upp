package com.milosz.mergeupp;

import java.util.List;

public record DataSheet(String name, String path, List<Object[]> data) {}
