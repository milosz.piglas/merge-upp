package com.milosz.mergeupp;

import java.io.File;
import java.util.Arrays;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.SwingWorker;

public class ExcelLoader extends SwingWorker<Integer, String> {

  private static final Logger LOG = LogManager.getLogManager().getLogger("");

  private final SheetModel model;
  private final File file;

  public ExcelLoader(SheetModel model, File file) {
    this.model = model;
    this.file = file;
  }

  @Override
  protected Integer doInBackground() throws Exception {
    try {
      final var input = MergeUtils.loadRows(file);
      for (final var ds : input) {
        if (ds.name().startsWith("Objasnienia")) {
          continue;
        }
        final var years = MergeUtils.loadYears(ds);
        for (final var ent : years.entrySet()) {
          final var pi =
              new PubInfo(
                  ds.path(), ds.name(), ent.getKey(), ent.getValue().size(), ent.getValue());
          LOG.info(
              "Loaded sheet: "
                  + ds.name()
                  + " file: "
                  + ds.path()
                  + " year: "
                  + ent.getKey()
                  + " rows: "
                  + ent.getValue().size());
          model.add(pi);
        }
      }
      LOG.info("Loaded file " + file.getAbsolutePath());
      return input.size();
    } catch (final Exception e) {
      LOG.info(
          "Load error "
              + e.getLocalizedMessage()
              + " stack: "
              + Arrays.stream(e.getStackTrace())
                  .map(String::valueOf)
                  .collect(Collectors.joining("#")));
      e.printStackTrace();
    }
    return -1;
  }
}
