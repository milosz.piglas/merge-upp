package com.milosz.mergeupp;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class SheetModel extends AbstractTableModel {
  private final List<PubInfo> sections = new ArrayList<>();

  private final String[] columns = {"Plik", "Arkusz", "Rok publikacji", "Liczba pozycji"};
  private final String[] fields = {"path", "sheet", "year", "rows"};

  @Override
  public int getRowCount() {
    return sections.size();
  }

  @Override
  public int getColumnCount() {
    return columns.length;
  }

  @Override
  public String getColumnName(int column) {
    return columns[column];
  }

  @Override
  public Object getValueAt(int row, int column) {
    try {
      final var rec = sections.get(row);
      final var m = PubInfo.class.getMethod(fields[column]);
      return m.invoke(rec);
    } catch (final Exception e) {
      throw new RuntimeException(e);
    }
  }

  public void add(PubInfo info) {
    sections.add(info);
    fireTableDataChanged();
  }

  public List<PubInfo> getSections() {
    return sections;
  }
}
