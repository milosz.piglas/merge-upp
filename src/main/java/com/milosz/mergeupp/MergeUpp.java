package com.milosz.mergeupp;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

public class MergeUpp extends JFrame {

  private static Logger LOG;

  static {
    final var mgr = LogManager.getLogManager();
    try {
      mgr.readConfiguration(MergeUpp.class.getResourceAsStream("log.properties"));
      LOG = mgr.getLogger("");
    } catch (SecurityException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  private File lastDir = new File(System.getProperty("user.home"));

  public MergeUpp() throws HeadlessException {
    super("Merge UPP");
  }

  void start() {
    LOG.info("START");
    final var panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
    final var model = new SheetModel();
    final var table = new JTable(model);
    panel.add(new JScrollPane(table));
    final var btnPanel = new JPanel();
    btnPanel.setLayout(new BoxLayout(btnPanel, BoxLayout.LINE_AXIS));
    final var addBtn = new JButton("Dodaj");
    addBtn.addActionListener(
        (e) -> {
          final var dialog = new JFileChooser(lastDir);
          dialog.setFileFilter(new FileNameExtensionFilter("Excel 2007", "xlsx"));
          final var state = dialog.showOpenDialog(panel);
          if (state == JFileChooser.APPROVE_OPTION) {
            final var f = dialog.getSelectedFile();
            if (!f.getParentFile().equals(lastDir)) {
              lastDir = f.getParentFile();
            }
            LOG.info("Loading file " + f.getAbsolutePath());
            final var loader = new ExcelLoader(model, f);
            loader.execute();
          }
        });
    final var mergeBtn = new JButton("Połącz");
    mergeBtn.addActionListener(
        (e) -> {
          final var homeDir = System.getProperty("user.home");
          LOG.info("Home directory: " + homeDir);
          final var dialog = new JFileChooser();
          dialog.setSelectedFile(new File(homeDir, "merge-out.xlsx"));
          dialog.setFileFilter(new FileNameExtensionFilter("Excel 2007", "xlsx"));
          final var state = dialog.showSaveDialog(panel);
          if (state == JFileChooser.APPROVE_OPTION) {
            var f = dialog.getSelectedFile();
            if (!f.getName().endsWith(".xlsx")) {
              f = new File(f.getParentFile(), f.getName() + ".xlsx");
            }
            if (f.exists()) {
              final var opt =
                  JOptionPane.showConfirmDialog(
                      panel,
                      "Plik " + f.getAbsolutePath() + " już istnieje. Czy chcesz go zastąpić?");
              if (opt != JOptionPane.YES_OPTION) {
                return;
              }
              LOG.info("Replacing file " + f.getAbsolutePath());
            }
            final var saver = new ExcelSaver(f, model);
            saver.execute();
            JOptionPane.showMessageDialog(panel, "Zapisany plik " + f.getAbsolutePath());
          }
        });

    btnPanel.add(addBtn);
    btnPanel.add(mergeBtn);
    panel.add(btnPanel);
    add(panel);
    setMinimumSize(new Dimension(800, 800));
    pack();
    setVisible(true);
    //    JOptionPane.showMessageDialog(
    //        panel,
    //        """
    //    	<html><body><ul><li><p>Autor w żadnym wypadku nie ponosi odpowiedzialności za
    // bezpośrednie lub pośrednie, majątkowe lub osobiste szkody dowolnego<br> rodzaju powstałe w
    // związku z eksploatacją programu, w tym między innymi za szkody związane z utratą wartości
    // firmy, przerwami w pracy,<br> awarią bądź zakłóceniami pracy komputera, utratą danych ani za
    // wszelkie inne szkody gospodarcze</p></li><li><p>Program ma charakter utworu nieukończonego i
    // wciąż udoskonalanego. Nie stanowi on utworu gotowego i dlatego<br> może posiadać wady lub
    // błędy. Z powyższych względów program przekazuje się<br> w postaci, w jakiej jest
    // udostępniany, i bez jakichkolwiek gwarancji dotyczących programu, w tym między innymi bez
    // gwarancji przydatności handlowej,<br> gwarancji przydatności do określonego celu, gwarancji
    // braku wad lub błędów, gwarancji dokładności</p></li></ul></body></html>""",
    //        "Informacja",
    //        JOptionPane.INFORMATION_MESSAGE);
    addWindowListener(
        new WindowAdapter() {
          @Override
          public void windowClosing(WindowEvent e) {
            LOG.info("Close");
            System.exit(0);
          }
        });
  }

  public static void main(String[] args) {
    SwingUtilities.invokeLater(
        () -> {
          UIManager.put("OptionPane.yesButtonText", "Tak");
          UIManager.put("OptionPane.noButtonText", "Nie");
          UIManager.put("OptionPane.cancelButtonText", "Anuluj");
          final var mu = new MergeUpp();
          mu.start();
        });
  }
}
